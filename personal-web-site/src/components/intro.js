import React, { Component } from 'react';
import '../css/intro.css';

class Intro extends Component {
  render() {
    return (
      <div className="intro">
        <img src={require("../images/forest.jpg")} className="introImage img-responsive" alt="nothing is present here"/>
        <div className="row">
          <div className="introText col-xs-4 col-xs-offset-4">
            <p>
              Hello!! Welcome to my website
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 col-xs-offset-4 aboutBtn">
            <p onClick={clickBtn}>About Me</p>
          </div>
        </div>
      </div>
    );
  }
}

function clickBtn () {
  window.location.pathname = "/projects";
}
export default Intro;