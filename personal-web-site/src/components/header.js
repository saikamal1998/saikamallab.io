import React, { Component } from 'react';
import {NavLink}from 'react-router-dom';
import '../css/header.css';

class Header extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-sm-12">
          <div className = "navbar navbar-inverse">
            <div className="navbar-header">
              <NavLink to="/">
                <img className="logo navbar-brand" src={require("../images/sklogo.jpg")} alt="nothing is present"/>
              </NavLink>
              <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <ul className="nav navbar-nav navbar-right navbar-collapse collapse" id="menu">
              <li><NavLink to="/about" activeClassName="activeLink">About Me</NavLink></li>
              <li><NavLink to="/projects" activeClassName="activeLink">Projects</NavLink></li>
              <li><NavLink to="/tunnelRush" activeClassName="activeLink">Tunnel Rush</NavLink></li>
            </ul>
          </div>
        </div>
      </div>
      );
    }
  }

  export default Header;