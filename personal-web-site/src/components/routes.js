import React, { Component } from 'react';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import Route, {IndexRoute} from 'react-router-dom/Route';
import Intro from '../components/intro';
import About from '../components/about';

class Routes extends Component {
  render () {
    return (
      <div>
        <Route path="/" exact component={Intro} />
        <Route path="/about" exact component={About} />
        <Route path="/projects" exact component={About} />
        <Route path="/tunnelRush" exact component={About} />
      </div>
    );
  }
}

export default Routes;

// {(window.location.pathname === "/") ? null : <Route path="/" exact component={App} />}
// {(window.location.pathname === "/about") ? null : <Route path="/about" exact component={AboutMe} />}
// {(window.location.pathname === "/projects") ? null : <Route path="/projects" exact component={Projects} />}
// {(window.location.pathname === "/tunnelRush") ? null : <Route path="/tunnelRush" exact component={TunnelRush} />}