import React, { Component } from 'react';
import {BrowserRouter as Router}from 'react-router-dom';
import '../css/footer.css';

class Footer extends Component {
  render() {
    return (
      <div className="footerDiv container-fluid">
        <Router>
          <footer class="w3-container w3-teal w3-center w3-margin-top">
            <p>You can find me on social media.</p>
            <a href="https://www.facebook.com/sai.kamal.50">
              <i className="fa fa-facebook-official w3-hover-opacity" />
            </a>
            <a href="https://www.linkedin.com/in/sai-kamal-amuluru-533580154/">
              <i className="fa fa-linkedin w3-hover-opacity" />
            </a>
          </footer>
        </Router>
      </div>
    );
  }
}

export default Footer;