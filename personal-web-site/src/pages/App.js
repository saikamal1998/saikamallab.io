import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import './App.css';
import Header from '../components/header';
import Intro from '../components/intro';
import About from '../components/about';
import Projects from '../components/projects';
import TunnelRush from '../components/tunnelRush';
import Footer from '../components/footer';
import "../../node_modules/jquery/dist/jquery.min.js";
import "../../node_modules/bootstrap/dist/js/bootstrap.min.js";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
            <Route path="/" exact component={Intro} />
            <Route path="/about" exact component={About} />
            <Route path="/projects" exact component={Projects} />
            <Route path="/tunnelRush" exact component={TunnelRush} />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;